# Contributor: Jonas <3426-spameier@users.gitlab.alpinelinux.org>
# Maintainer: Jonas <3426-spameier@users.gitlab.alpinelinux.org>
pkgname=texstudio
pkgver=4.8.2
pkgrel=0
pkgdesc="A fully featured editor for LaTeX documents"
url="https://www.texstudio.org"
# disabled elsewhere due to huge size, request if you need it
arch="aarch64 x86_64"
license="GPL-3.0-or-later"
makedepends="
	cmake
	poppler-qt5-dev
	qt6-qt5compat-dev
	qt6-qtbase-dev
	qt6-qtdeclarative-dev
	qt6-qtsvg-dev
	qt6-qttools-dev
	qtermwidget-dev
	quazip-dev
	samurai
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/texstudio-org/texstudio/archive/$pkgver.tar.gz
	qt6.patch
	"
options="!check" # tests fail to build

build() {
	cmake -B build -G Ninja -Wno-dev \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DQT_VERSION_MAJOR=6
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
6f67ccf38963f678e3879224d66fb58842480d0215f3e1a5a7d136cd58752f0c994415d692e55f6616a675beddb9e70d5b06b17fd7fc8d43ba96beef4accff9e  texstudio-4.8.2.tar.gz
60380c7e962ea9deb9117c95153dc7f827b0534adeeb59b6de123d57264b74dba3c960744433686c68fac34f11d788125ba7bc08d36882b10f1282bfa597829d  qt6.patch
"
