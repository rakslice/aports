# Maintainer: Patrycja Rosa <alpine@ptrcnull.me>
pkgname=py3-pydantic-core
pkgver=2.23.4
pkgrel=0
pkgdesc="Core validation logic for pydantic written in rust"
url="https://github.com/pydantic/pydantic-core"
arch="all"
license="MIT"
depends="py3-typing-extensions"
makedepends="py3-gpep517 py3-maturin py3-wheel py3-installer cargo"
checkdepends="
	py3-pytest
	py3-pytest-benchmark
	py3-pytest-mock
	py3-pytest-timeout
	py3-hypothesis
	py3-dirty-equals
	py3-tzdata
	"
subpackages="$pkgname-pyc"
source="https://github.com/pydantic/pydantic-core/archive/refs/tags/v$pkgver/py3-pydantic-core-$pkgver.tar.gz"
builddir="$srcdir/pydantic-core-$pkgver"
options="net" # cargo crates

prepare() {
	default_prepare
	cargo fetch --locked
}

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--config-json '{"build-args": "--frozen"}' \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
595c50346e6e4522b9b75cbe07cdfacf3c714ea28d1bee1b78d0128517e5337563ca360db9846944518bbec0a279b666db98397a9753f5809ac5f2900dd6e101  py3-pydantic-core-2.23.4.tar.gz
"
