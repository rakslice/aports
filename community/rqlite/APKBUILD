# Maintainer: Celeste <cielesti@protonmail.com>
pkgname=rqlite
pkgver=8.30.3
pkgrel=0
pkgdesc="Lightweight, distributed relational database built on SQLite"
url="https://github.com/rqlite/rqlite"
arch="all"
license="MIT"
makedepends="go sqlite-dev"
checkdepends="python3 py3-requests"
options="chmod-clean net !check" # a bunch of them fail due to runtime requirements
subpackages="
	$pkgname-doc
	$pkgname-bench
	$pkgname-client
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/rqlite/rqlite/archive/refs/tags/v$pkgver.tar.gz"

export GOFLAGS="$GOFLAGS -tags=libsqlite3"
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build ./cmd/rqbench &
	go build ./cmd/rqlite &
	go build ./cmd/rqlited &
	wait
}

check() {
	RQLITED_PATH="$builddir"/rqlited python3 system_test/full_system_test.py
}

package() {
	install -Dm755 rqbench rqlite rqlited \
		-t "$pkgdir"/usr/bin/

	install -Dm644 DOC/*.md \
		-t "$pkgdir"/usr/share/doc/$pkgname/

	install -Dm644 LICENSE \
		-t "$pkgdir"/usr/share/licenses/$pkgname/
}

bench() {
	pkgdesc="$pkgdesc (benchmark utility)"

	amove usr/bin/rqbench
}

client() {
	pkgdesc="$pkgdesc (client)"

	amove usr/bin/rqlite
}

sha512sums="
5a5e3e4864b40a18122de8352d7ac3877754b4070160daf0ae23c1b173fc0a19b823e182fc980b386dfb3356d98531d96bfe93472bdca5228d8da71df81b81a3  rqlite-8.30.3.tar.gz
"
