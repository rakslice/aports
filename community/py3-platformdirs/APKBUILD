# Contributor: Duncan Bellamy <dunk@denkimushi.com>
# Maintainer: Duncan Bellamy <dunk@denkimushi.com>
pkgname=py3-platformdirs
pkgver=4.3.1
pkgrel=0
pkgdesc="Module for determining appropriate platform-specific dirs"
url="https://github.com/platformdirs/platformdirs"
arch="noarch"
license="MIT"
makedepends="py3-gpep517 py3-hatchling py3-hatch-vcs"
checkdepends="py3-appdirs py3-pytest py3-pytest-mock"
subpackages="$pkgname-pyc"
source="https://files.pythonhosted.org/packages/source/p/platformdirs/platformdirs-$pkgver.tar.gz"
builddir="$srcdir/platformdirs-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" .dist/*.whl
}

sha512sums="
6d4cd808190808bab5a90f44d9fe2fda0d624c7a76faf9f4a4a6093cd098a28b79782dbf895dcd0eeb82778aa094deeb0959fcbe25bdd93818a47b9e197b6299  platformdirs-4.3.1.tar.gz
"
